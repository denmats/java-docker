package com.denmats.springbootdocker.model;

import jakarta.persistence.*;
import lombok.*;

/**
 * @author Denys Matsuiev
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private  String title;
    private  String text;
}
