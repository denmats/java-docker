package com.denmats.springbootdocker.repository;

import com.denmats.springbootdocker.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Denys Matsuiev
 */
@Repository
public interface PostRepository extends JpaRepository<Post,Integer> {
}
