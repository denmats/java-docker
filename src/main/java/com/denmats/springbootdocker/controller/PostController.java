package com.denmats.springbootdocker.controller;

import com.denmats.springbootdocker.model.Post;
import com.denmats.springbootdocker.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Denys Matsuiev
 */
@RestController
@RequiredArgsConstructor
class PostController {

    private final PostRepository postRepository;

    @GetMapping("/")
    public String welcome(){
        return "welcome!";
    }

    @GetMapping("/posts")
    public List<Post> posts(){
        return postRepository.findAll();
    }
}
