FROM eclipse-temurin:17-jdk-jammy as base
RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y inotify-tools dos2unix
WORKDIR /app
COPY .mvn/ .mvn
COPY mvnw pom.xml ./
RUN ./mvnw dependency:go-offline
COPY src ./src

#FROM base as development
#CMD ["./mvnw", "spring-boot:run"]
